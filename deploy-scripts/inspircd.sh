#!/bin/bash

# Version: 0.01
# Author: MrRandom
# Currently untested

# USAGE
# Installs inspircd to ~/ircd/
# to run simply do:
# ./inspircd.sh

cd $HOME

# Get inspircd and extract
wget https://github.com/inspircd/inspircd/archive/v2.0.12.tar.gz
tar xvfz
tar xvfz v2.0.12.tar.gz
cd inspircd-2.0.12/

# configure inspircd
./configure --enable-epoll --enable-gnutls --prefix=$HOME/ircd
make
make install
