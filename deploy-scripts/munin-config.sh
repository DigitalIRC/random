#!/bin/bash

# Version: 0.01-DEV
# Author: MrRandom
# Un-tested

muninconf=/etc/munin/


if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
	cd $muninconf
	
	# Remove default config
	rm munin-node.conf
	
	# Move config file
	mv /tmp/munin-node.conf $muninconf/munin-node.conf # Load default munin config with allow for munin server to connect
	
	# Load modules
	cd $muninconf/plugins
	ln -s /usr/share/munin/plugins/apt_all ./apt_all
	
	munin-node-config --shell
	
fi