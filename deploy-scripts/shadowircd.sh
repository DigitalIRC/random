#!/bin/bash

# Version: 0.01
# Author: MrRandom
# Currently untested

cd $HOME

# Check bison & flex are installed

haveBison=$(dpkg –l | grep "flex" | grep -q "A fast lexical analyzer generator.")
haveFlex=$(dpkg –l | grep "bison" | grep -q "YACC-compatible parser generator")

if [ $haveBison -eq 0 ] && [ $haveFlex -eq 0 ]; then
    echo please install flex and bison before running this script
    exit 0
fi

# Start install

cd $HOME
wget https://github.com/shadowircd/shadowircd/archive/release/6.3.zip
unzip 6.3.zip
cd shadowircd-release-6.3/
./configure --enable-ipv6
make
make install

echo Please now install your ircd.conf in $HOME/ircd/etc/
