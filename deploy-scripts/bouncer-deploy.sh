#!/bin/bash 

# Version: 0.01-DEV
# Author: MrRandom
# Un-tested

zncpw=$S1
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
	apt-get install znc znc-extra znc-dev
	/usr/sbin/useradd znc
	echo $zncpw | passwd znc --stdin
	cd /home/ircd
	#wget znc-backup.tar.gz
	#tar xvfz znc-backup.tar.gz
fi