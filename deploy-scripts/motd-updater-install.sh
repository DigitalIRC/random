#!/bin/bash

# Version: 0.01-DEV
# Author: MrRandom
# Un-tested

servername=$1

cd $HOME

# Get updater files

wget https://raw.github.com/DigitalIRC/random/master/IRC/updater.sh
chmod +x updater.sh

#write out current crontab
crontab -l > crontab.new
#echo new cron into cron file
echo "@daily bash /home/ircd/updater.sh ${servername} >/dev/null 2>&1" >> crontab.new
#install new cron file
crontab crontab.new
rm crontab.new