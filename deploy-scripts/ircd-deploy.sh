#!/bin/bash

# Version: 0.02-DEV
# Author: MrRandom
# Un-tested

ircpw=$1
servername=$2

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
	# Add ircd user
	/usr/sbin/useradd ircd
	echo $ircpw | passwd ircd --stdin
	cd /home/ircd
	# Setup IRC user
	echo "Please manually copy unrealircd.conf"
    if [ $3 == "inspircd" ]; then
        apt-get install g++ libgnutls-dev libgnutls-dev gnutls-bin pkg-config # Install packages inspircd needs to run
        sudo -u ircd /tmp/inspircd.sh
    else if [ $3 == "unreal" ]; then
        sudo -u ircd /tmp/unreal.sh
    else if [ $3 == "shadowircd" ]; then
        sudo -u ircd /tmp/shadowircd.sh
    fi
	sudo -u ircd /tmp/motd-updater-install.sh $servername

fi