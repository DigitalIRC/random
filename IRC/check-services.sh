#!/bin/bash

DIRECTORY=$HOME/services-backup
ATHEME=$HOME/atheme
PASS=$1
REPO=$2

echo "/version services.int" > /tmp/ii-ircd/127.0.0.1/in
sleep 3s

if [ $(tail -1 /tmp/ii-ircd/127.0.0.1/out | grep -cP "atheme.*services.int") != 1 ]; then
    echo "/join #help" > /tmp/ii-ircd/127.0.0.1/in
    echo "/privmsg #help :WARNING Services have been detected to be offline! Starting backup services" > /tmp/ii-ircd/127.0.0.1/in
    if [ ! -d "$DIRECTORY" ]; then
            cd $HOME
            git clone $REPO
    fi
    cd $DIRECTORY
    git pull
    openssl aes128 -in atheme.tar.bz.enc -out atheme.tar.bz -d -a -k $PASS
    tar -xjf atheme.tar.bz
    mv atheme.db $ATHEME/etc/atheme.db
    $ATHEME/bin/atheme-services
    echo "/privmsg #help :Backup Services started! from database backup from $(date +%D --date="-1 day")" > /tmp/ii-ircd/127.0.0.1/in
    echo "/part #help" > /tmp/ii-ircd/127.0.0.1/in
fi
