#!/bin/bash

# Creates an encryped backup of the services database and pushes it to a git repo
# Author: MrRandom
# Version: 1.2.1-Live

# USAGE:
# run using crontab with the following line:
# @daily bash /home/ircd/backup.sh P4$$w0rd git@gitrepo:user/blah >/dev/null 2>&1
# You can of course run it more than once a day depending on how often you want to backup.

DIRECTORY=$HOME/services-backup
ATHEME=$HOME/atheme

if [ ! -d "$DIRECTORY" ]; then
        cd $HOME
        git clone $2
else
        cd $DIRECTORY
        git pull
        git add *
        cp $ATHEME/etc/services.db $DIRECTORY/atheme.db
        cd $DIRECTORY
        tar -cj atheme.db | openssl enc -aes128 -salt -out atheme.tar.bz.enc -e -a -k $1
        rm atheme.db
        git commit -a --message="Update services DB $(date +%D) $(date +%T)"
        git push
fi