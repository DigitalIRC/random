#!/bin/bash

# Version: 1.0
# Author: MrRandom

# Usage
# Generates motd for a specified server.
# To run use the following line
# ./updatemotd serverName
# serverName MUST be the name of a valid server with information in the git repo
# e.g. ./updatemotd binary would work since binary.txt exists in the motd git repo
# but ./updatemotd blah wouldn't work since the files blah .txt do not exist

SERVER=$1 # Take CLI server name
UNREAL=$HOME/unreal # Use common unreal path
DIRECTORY=$HOME/DigitalIRC-motd # Directory repo would be cloned into
if [ ! -d "$DIRECTORY" ]; then # Check if git repo has been cloned already
        cd $HOME # Store that shit in home
        git clone git://github.com/DigitalIRC/DigitalIRC-motd.git # clone that shit like star wars
        cd $DIRECTORY # move into directory
        chmod +x makeArtExperimental.sh # make sure its executable
else
        cd $DIRECTORY # move into directory
        git pull # Pull harder
		chmod +x makeArtExperimental.sh # make sure its executable
fi

		bash $DIRECTORY/makeArtExperimental.sh $SERVER # Lets make these motd's
        cp $DIRECTORY/assembled/$SERVER.motd $UNREAL/ircd.motd # move new MOTD to unreal's directory
        cp $DIRECTORY/assembled/ircd.rules $UNREAL/ircd.rules # move rules to unreal's directory
        cd $UNREAL # move to unreals directory
        bash unreal rehash # rehash the unreal server so new motd & rules are loaded
