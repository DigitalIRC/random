#!/bin/bash

# Version: 0.1-DEV
# Author: MrRandom
# Un-tested

if [[ $EUID -e 0 ]]; then
    apt-get update 
    apt-get upgrade
    apt-get install gcc gcc++ libgnutls-dev gnutls-bin pkg-config

    id $username &> /dev/null
    if [ $? -eq 0 ]; then
        echo "$username exists... changing password."
    else
        echo "$username doesn't exist. Creating $username account." 
        /usr/sbin/useradd $username
    fi

    read -p "Enter password for $username user: " -s ircpw
    read -p "Password again: " -s ircpw1
    if [ $ircpw != $ircpw1 ]; then
        echo "Passwords do not match"
        exit    
    else 
        echo "Passwords match. Changing password."
        echo $ircpw | passwd $username --stdin
    fi 
    sudo -u ircd -H sh -c "bash /tmp/ircd-install.sh; exit" 
else
    username=ircd
    VERSION=2.0.16
    user=$(whoami)
    if [$user -e $username]; then
        cd $HOME

        # Get inspircd and extract
        wget https://github.com/inspircd/inspircd/archive/v$VERSION.zip -O inspircd.zip
        unzip inspircd.zip
        cd inspircd-$VERSION/

        # configure inspircd
        ./configure --enable-gnutls --prefix=$HOME/inspircd
        make
        make install
    fi
fi

