#!/bin/bash

# Version 0.01-DEV
# ALL SCRIPTS ARE CURRENTLY UNTESTED. DO NOT USE.

# Usage
# Used to auto deploy servers for the DigitalIRC network.
# To run use use the follwoing line:
# ./serverdeploy.sh servername ircdUserPass ircdType
#
# Valid ircdTypes: unreal, inspircd, shadowircd
# If you also wish to setup znc simply add the password you
# want for znc user after ircdUserPass in the example line 
# above


servername=$1
ircpw=$2
irctype=$3
zncpw=$4

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
	# Update the server. Assuming debian or similar
	apt-get update
	apt-get --assume-yes upgrade
	
	# Get normal packages

	apt-get --assume-yes install htop fail2ban oidentd ntp-date libssl-dev bash-completion munin-node libcache-cache-perl flex bison

	
	# Get nessiary files
	cd /tmp
	wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-scripts/ircd-deploy.sh" # IRCd user setup
	if [ -z "$3" ]; then # check we have a znc user to create
		wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-scripts/bouncer-deploy.sh" # znc user setup
	fi
	wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-scripts/unreal.sh" # unrealircd installed
	wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-scripts/motd-updater-install.sh" # motd system installer for unreal
	wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-files/sshd.patch" # change ssh port & disable root login
	wget "https://raw.github.com/DigitalIRC/random/master/IRC/deploy-files/munin-node.conf" # configure munin-node
	
	# Change permissions & make executable
	chmod -R 777 /tmp/*
	chmod -R +x /tmp/*
	
	# Patch SSHd
	cd /etc/ssh
	patch -p1 < /tmp/sshd.patch
	service ssh rehash
	
	# Execute deploy scripts
	bash /tmp/ircd-deploy.sh $ircpw $servername $irctype
	bash /tmp/munin-config.sh
	if [ -z "$4" ];; then
		bash /tmp/bouncer-deploy.sh $zncpw
	fi
	
	# Add ntp-date to daily tasks
	cd /root
	crontab -l > crontab.new
	#echo new cron into cron file
	echo "@daily ntpdate-debian pool.ntp.org >/dev/null 2>&1" >> crontab.new
	#install new cron file
	crontab crontab.new
	rm crontab.new
	
fi

